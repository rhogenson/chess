package com.hamerschag3a.chess;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgcodecs.Imgcodecs;

public class ImageMatcher {

	private Hashtable<String,ArrayList<Pair<Mat,MatOfKeyPoint>>> features;
	private char[] colors = {'b', 'w'};
	private char[] names = {'b', 'k', 'n', 'p', 'q', 'r'};

	public ImageMatcher()
	{
		FeatureDetector f = FeatureDetector.create(FeatureDetector.ORB);
		DescriptorExtractor d = DescriptorExtractor.create(DescriptorExtractor.ORB);
		features = new Hashtable<String,ArrayList<Pair<Mat,MatOfKeyPoint>>>();

		for (char color : colors)
		{
			for (char name : names)
			{
				for (int i = 0; i < 100; ++i)
				{
					String filename = Character.toString(color) + Character.toString(name);
					Mat descriptors = new Mat();
					Mat image = Imgcodecs.imread("src/" + filename + i + ".jpg");
					if (image.empty())
						break;
					MatOfKeyPoint kp = new MatOfKeyPoint();
					f.detect(image, kp);
					d.compute(image, kp, descriptors);
					if (features.get(filename) == null)
						features.put(filename, new ArrayList<Pair<Mat,MatOfKeyPoint>>());
					features.get(filename).add(new Pair<Mat,MatOfKeyPoint>(descriptors, kp));
				}
			}
		}
	}

	String classify(Mat image)
	{
		/*if (image.empty())
			System.out.println("we've got a problem");*/
		DescriptorMatcher dm = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
		FeatureDetector f = FeatureDetector.create(FeatureDetector.ORB);
		DescriptorExtractor de = DescriptorExtractor.create(DescriptorExtractor.ORB);
		Mat descriptors = new Mat();
		MatOfKeyPoint kp = new MatOfKeyPoint();
		f.detect(image, kp);
		de.compute(image, kp, descriptors);
		if (check_blank(image))
			return " e";
		//System.out.println(descriptors.rows());

		int min_score = -1;
		String min_name = "";
		for (char color : colors)
		{
			for (char name : names)
			{
				String file = Character.toString(color) + Character.toString(name);
				int score = 0;
				if (!features.containsKey(file))
					System.out.println("t"+file);
				for (int img = 0; img < features.get(file).size(); ++img)
				{
					ArrayList<MatOfDMatch> matches_two = new ArrayList<MatOfDMatch>();
					dm.knnMatch(descriptors, features.get(file).get(img).first(), matches_two, 2);
					ArrayList<DMatch> matches_one = new ArrayList<DMatch>();
					for (int i = 0; i < matches_two.size(); ++i)
					{
						if (matches_two.get(i).toList().get(0).distance < matches_two.get(i).toList().get(1).distance * 0.8)
							matches_one.add(matches_two.get(i).toList().get(0));
					}
					int low = 12;
					if (matches_one.size() < low)
						low = matches_one.size();
					for (int i = 0; i < low; ++i)
					{
						score += matches_one.get(i).distance;
					}
				}
				if (min_score == -1 || score < min_score) {
					min_score = score;
					min_name = file;
				}
			}
		}
		return min_name;
	}
	
	private boolean check_blank(Mat image)
	{
		Mat small_image = image.submat(image.rows() / 4, image.rows() * 3 / 4,
									   image.cols() / 4, image.cols() * 3 / 4);
		if (small_image.empty())
			System.out.println("ao");
		Mat gray = small_image;
		ArrayList<Double> a = new ArrayList<Double>();
		for (int row = 0; row < gray.rows(); ++row) {
			for (int col = 0; col < gray.cols(); ++col) {
				double sum = 0;
				for (int i = 0; i < gray.get(row, col).length; ++i)
					sum += gray.get(row, col)[i];
				a.add(sum);
			}
		}
		Collections.sort(a);
		//System.out.println(a.get(a.size() * 4 / 5) - a.get(a.size() / 5));
		if (a.get(a.size() * 3 / 4) - a.get(a.size() / 4) < 40)
			return true;
		return false;
	}
}