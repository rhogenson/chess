package com.hamerschag3a.chess;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class GridProcess {
	
	public static Mat buf_to_mat(BufferedImage in)
	{
		Mat out;
        byte[] data;
        int r, g, b;

        if(in.getType() == BufferedImage.TYPE_INT_RGB)
        {
            out = new Mat(in.getHeight(), in.getWidth(), CvType.CV_8UC3);
            data = new byte[in.getWidth() * in.getHeight() * (int)out.elemSize()];
            int[] dataBuff = in.getRGB(0, 0, in.getWidth(), in.getHeight(), null, 0, in.getWidth());
            for(int i = 0; i < dataBuff.length; i++)
            {
                data[i*3] = (byte) ((dataBuff[i] >> 16) & 0xFF);
                data[i*3 + 1] = (byte) ((dataBuff[i] >> 8) & 0xFF);
                data[i*3 + 2] = (byte) ((dataBuff[i] >> 0) & 0xFF);
            }
        }
        else
        {
            out = new Mat(in.getHeight(), in.getWidth(), CvType.CV_8UC1);
            data = new byte[in.getHeight() * in.getWidth() * (int)out.elemSize()];
            int[] dataBuff = in.getRGB(0, 0, in.getWidth(), in.getHeight(), null, 0, in.getWidth());
            for(int i = 0; i < dataBuff.length; i++)
            {
              r = (byte) ((dataBuff[i] >> 16) & 0xFF);
              g = (byte) ((dataBuff[i] >> 8) & 0xFF);
              b = (byte) ((dataBuff[i] >> 0) & 0xFF);
              data[i] = (byte)((0.21 * r) + (0.71 * g) + (0.07 * b)); //luminosity
            }
         }
         out.put(0, 0, data);
         if (out.empty())
        	 System.out.println("still didn't work");
         return out;
	}
	
	public static BufferedImage mat_to_img(Mat in)
    {
        BufferedImage out;
        byte[] data = new byte[in.cols() * in.rows() * (int)in.elemSize()];
        int type;
        in.get(0, 0, data);

        if(in.channels() == 1)
            type = BufferedImage.TYPE_BYTE_GRAY;
        else
            type = BufferedImage.TYPE_3BYTE_BGR;

        out = new BufferedImage(in.cols(), in.rows(), type);

        out.getRaster().setDataElements(0, 0, in.cols(), in.rows(), data);
        return out;
    }
	
	static int x_mean(ArrayList<Point> nums, int max)
	{
		int total = 0;
		int sum = 0;
		for (int i = 0; i < nums.size(); ++i)
		{
			//if (nums.get(i).y != max && nums.get(i).x != max)
			{
				total += 1;
				sum += nums.get(i).x;
			}
		}
		if (total != 0)
			return sum / total;
		return 0;
	}
	
	static int y_mean(ArrayList<Point> nums, int max)
	{
		int total = 0;
		int sum = 0;
		for (int i = 0; i < nums.size(); ++i)
		{
			//if (nums.get(i).y != max && nums.get(i).x != max)
			{
				total += 1;
				sum += nums.get(i).y;
			}
		}
		if (total != 0)
			return sum / total;
		return 0;
	}
	
	private static int numerator(ArrayList<Point> nums, int max)
	{
		int sum = 0;
		int x_bar = x_mean(nums, max);
		int y_bar = y_mean(nums, max);
		for (int i = 0; i < nums.size(); ++i)
		{
			//if (nums.get(i).y != max && nums.get(i).x != max)
			{
				sum += (nums.get(i).x - x_bar) * (nums.get(i).y - y_bar);
			}
		}
		return sum;
	}
	
	private static int denominator(ArrayList<Point> nums, int max)
	{
		int sum = 0;
		int x_bar = x_mean(nums, max);
		for (int i = 0; i < nums.size(); ++i)
		{
			//if (nums.get(i).y != max && nums.get(i).x != max)
				sum += (nums.get(i).x - x_bar) * (nums.get(i).x - x_bar);
		}
		return sum;
	}
	
	/* Find the intersection point between two lines */
	private static Point intersection(double a_1, double b_1, double a_2, double b_2)
	{
		double x = (a_1 - a_2) / (b_2 - b_1);
		double y = a_1 + x * b_1;
		assert y - a_2 + x * b_2 < 0.1;
		return new Point((int)x, (int)y);
	}
	
	private static boolean is_black(double[] a)
	{
		for (int i = 0; i < a.length; ++i)
			if (a[i] != 0)
				return false;
		return true;
	}
	
	private static void sort_x (ArrayList<Point> a)
	{
		for (int i = 0; i < a.size(); ++i) {
			for (int j = 0; j < a.size() - 1; ++j) {
				if (a.get(j).x > a.get(j + 1).x) {
					Point tmp = a.get(j);
					a.set(j, a.get(j + 1));
					a.set(j + 1, tmp);
				}
			}
		}
	}
	
	private static void sort_y (ArrayList<Point> a)
	{
		for (int i = 0; i < a.size(); ++i) {
			for (int j = 0; j < a.size() - 1; ++j) {
				if (a.get(j).y > a.get(j + 1).y) {
					Point tmp = a.get(j);
					a.set(j, a.get(j + 1));
					a.set(j + 1, tmp);
				}
			}
		}
	}
	
	private static void remove_out_x(ArrayList<Point> a)
	{
		sort_x(a);
		int q1 = a.get(a.size() / 4).x;
		int q3 = a.get(a.size() * 3 / 4).x;
		int iqr = q3 - q1;
		for (int i = 0; i < a.size(); ++i) {
			if (a.get(i).x < q1 - 1.5 * iqr || a.get(i).x > q3 + 1.5 * iqr) {
				a.remove(i);
				--i;
			}
		}
	}

	private static void remove_out_y(ArrayList<Point> a)
	{
		sort_y(a);
		int q1 = a.get(a.size() / 4).y;
		int q3 = a.get(a.size() * 3 / 4).y;
		int iqr = q3 - q1;
		for (int i = 0; i < a.size(); ++i) {
			if (a.get(i).y < q1 - 1.5 * iqr || a.get(i).y > q3 + 1.5 * iqr) {
				a.remove(i);
				--i;
			}
		}
	}

	public static ArrayList<Pair<Point,Point>> process_grid(Mat in_image)
	{
		ArrayList<Point> tops = new ArrayList<Point>(12);
		for (int i = 0; i < 12; ++i)
		{
			tops.add(new Point(i * in_image.cols() / 12, 0));
			while (tops.get(i).y < in_image.cols() && is_black(in_image.get(tops.get(i).y, tops.get(i).x)))
			{
				++tops.get(i).y;
			}

		}
		remove_out_y(tops);
		double t_beta_hat = (0. + numerator(tops, in_image.rows())) / denominator(tops, in_image.rows());
		double t_alpha_hat = y_mean(tops, in_image.rows()) - t_beta_hat * x_mean(tops, in_image.rows());
		
		ArrayList<Point> bots = new ArrayList<Point>(12);
		for (int i = 0; i < 12; ++i)
		{
			bots.add(new Point(i * in_image.cols() / 12, in_image.cols() - 1));
			while (bots.get(i).y > 0 && is_black(in_image.get(bots.get(i).y, bots.get(i).x)))
			{
				--bots.get(i).y;
			}

		}
		remove_out_y(bots);
		double b_beta_hat = (0. + numerator(bots, 0)) / denominator(bots, 0);
		double b_alpha_hat = y_mean(bots, 0) - b_beta_hat * x_mean(bots, 0);
		
		ArrayList<Point> rights = new ArrayList<Point>(12);
		for (int i = 0; i < 12; ++i)
		{
			rights.add(new Point(in_image.cols() - 1, i * in_image.rows() / 12));
			while (is_black(in_image.get(rights.get(i).y, rights.get(i).x)) && rights.get(i).x > 0)
			{
				--rights.get(i).x;
			}
			
		}
		remove_out_x(rights);
		double r_beta_hat = (0. + numerator(rights, 0)) / denominator(rights, 0);
		double r_alpha_hat = y_mean(rights, 0) - r_beta_hat * x_mean(rights, 0);
		
		ArrayList<Point> lefts = new ArrayList<Point>(12);
		for (int i = 0; i < 12; ++i)
		{
			lefts.add(new Point(0, i * in_image.rows() / 12));
			while (lefts.get(i).x < in_image.cols() && is_black(in_image.get(lefts.get(i).y, lefts.get(i).x)))
			{
				++lefts.get(i).x;
			}
			
		}
		remove_out_x(lefts);
		double l_beta_hat = (0. + numerator(lefts, in_image.cols())) / denominator(lefts, in_image.cols());
		double l_alpha_hat = (0. + y_mean(lefts, in_image.cols())) - l_beta_hat * x_mean(lefts, in_image.cols());
		
		
		/*
		 * Now that we have the equations for each line, we can calculate the coordinates of each corner of the
		 * grid by the intersection formula for linear equations.
		 */
		Point tl = intersection(t_alpha_hat, t_beta_hat, l_alpha_hat, l_beta_hat);

		Point tr = intersection(t_alpha_hat, t_beta_hat, r_alpha_hat, r_beta_hat);
		Point bl = intersection(b_alpha_hat, b_beta_hat, l_alpha_hat, l_beta_hat);
		Point br = intersection(b_alpha_hat, b_beta_hat, r_alpha_hat, r_beta_hat);
		
		/* Manual scale factors because this is bad */
		/*tl.y += 200;
		tr.y += 200;*/
		
		
		int total_width = (tr.x - tl.x + br.x - bl.x) / 2;
		int total_height = (br.y - tr.y + bl.y - tl.y) / 2;
		int top = (tl.y + tr.y) / 2;
		int left = (tl.x + bl.x) / 2;

		ArrayList<Pair<Point,Point>> outs = new ArrayList<Pair<Point,Point>>(64);
		for (int row = 0; row < 8; ++row)
		{
			for (int col = 0; col < 8; ++col)
			{
				//int y = (8 - row) * (l_beta_hat * row * total_height / 8 + l_alpha_hat 
				outs.add(new Pair<Point,Point>(new Point(col * total_width / 8 + left, row * total_height / 8 + top),
						 new Point(total_width / 8 + col * total_width / 8 + left,
								   total_height / 8 + row * total_height / 8 + top)));
			}
		}
		return outs;
	}
}
