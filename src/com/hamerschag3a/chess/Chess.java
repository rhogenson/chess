package com.hamerschag3a.chess;

import java.util.ArrayList;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

public class Chess {

	public static void main(String[] args) {
		 System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		 
		 /*Mat a = Imgcodecs.imread("src/testwr0.jpg");
		 ImageMatcher im = new ImageMatcher();
		 System.out.println(im.classify(a));*/
		 
		 run_board("src/board4.jpg");
		 
	}
	
	private static void run_board(String image)
	{
		  Mat img = Imgcodecs.imread(image);
		  Mat edges = new Mat();
		  Imgproc.cvtColor(img, edges, Imgproc.COLOR_BGR2GRAY );
		  

		  Imgproc.Canny(edges, edges, 100, 100*3);
		  
		  
		  
		  
		  /*JFrame editorFrame = new JFrame("Image Demo");
	        ImageIcon imageIcon = new ImageIcon(GridProcess.mat_to_img(edges));
	        JLabel jLabel = new JLabel();
	        jLabel.setIcon(imageIcon);
	        editorFrame.getContentPane().add(jLabel, BorderLayout.CENTER);

	        editorFrame.pack();
	        editorFrame.setLocationRelativeTo(null);
	        editorFrame.setVisible(true);*/
	        
	        
	        
	        
		  ArrayList<Pair<Point,Point>> locs = GridProcess.process_grid(edges);

		  ArrayList<Mat> sub_images = new ArrayList<Mat>(64);
		  for (int i = 0; i < 64; ++i) {
			  sub_images.add(img.submat(locs.get(i).first().y, locs.get(i).second().y,
					  								    locs.get(i).first().x,
					  								    locs.get(i).second().x));
			  //Imgcodecs.imwrite("src/" + i + ".jpg", sub_images.get(i));
		  }
		  ArrayList<String> letters = new ArrayList<String>(64);
		  ImageMatcher im = new ImageMatcher();
		  for (int i = 0; i < 64; ++i) {
			  String a = im.classify(sub_images.get(i));
			  if (a.length() != 2)
				  System.out.println("help, the thing is " + a);
			  letters.add(a);
		  }
		  Board b = new Board();
		  b.set_from_letters(letters);
		  System.out.println(b.to_fen());
	}
}