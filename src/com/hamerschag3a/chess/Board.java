package com.hamerschag3a.chess;

import java.util.ArrayList;
import java.util.List;

public class Board
{
	private List<List<Character>> board;
	private boolean white_to_move;

	public Board()
	{
		board = new ArrayList<List<Character>>();
		for (int i = 0; i < 8; ++i)
			board.add(new ArrayList<Character>());
		for (int i = 0; i < 8; ++i)
			for (int j = 0; j < 8; ++j)
				board.get(i).add(' ');
		white_to_move = true;
	}

	public void white_to_move()
	{
		white_to_move = true;
	}
	
	public void black_to_move()
	{
		white_to_move = false;
	}

	public String to_fen()
	{
		String output = "";
		for (int rank = 0; rank < 8; ++rank)
		{
			for (int file = 0; file < 8; ++file)
			{
				output += board.get(rank).get(file);
			}
			output += "/";
		}
		String better_output = "";
		for (int i = 0; i < output.length(); ++i)
		{
			int spaces = 0;
			while (i + spaces < output.length() && output.charAt(i + spaces) == ' ')
				++spaces;
			if (spaces != 0) {
				better_output += Integer.toString(spaces);
				i += spaces - 1;
			} else {
				better_output += output.charAt(i);
			}
		}
		better_output = better_output.substring(0, better_output.length() - 1);
		better_output += " ";
		if (white_to_move)
			better_output += "w";
		else
			better_output += "b";
		return better_output + " - - 0 0";
	}

	public void set_board(String board_str)
	{
		for (int rank = 0; rank < 8; ++rank)
		{
			for (int file = 0; file < 8; ++file)
			{
				board.get(rank).set(file, board_str.charAt(rank * 8 + file));
			}
		}
	}
	
	public void set_from_letters(ArrayList<String> board_str)
	{
		if (board_str.size() != 64)
			System.out.println("no");
		for (int rank = 0; rank < 8; ++rank)
		{
			for (int file = 0; file < 8; ++file) {
				board.get(rank).set(file, map_strs(board_str.get(rank * 8 + file)));
			}
		}
	}
	
	private Character map_strs(String in)
	{
		if (in.charAt(1) == 'e')
			return ' ';
		if (in.charAt(0) == 'w')
			return Character.toUpperCase(in.charAt(1));
		else
			return in.charAt(1);
	}
}