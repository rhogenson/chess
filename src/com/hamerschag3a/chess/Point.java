package com.hamerschag3a.chess;

public class Point {
	public int x;
	public int y;
	
	public Point(int new_x, int new_y)
	{
		x = new_x;
		y = new_y;
	}
}
