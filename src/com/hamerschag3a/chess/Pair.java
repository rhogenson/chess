package com.hamerschag3a.chess;

public class Pair<T,U> {
	private T one;
	private U two;
	
	public Pair(T first, U second)
	{
		one = first;
		two = second;
	}
	
	T first()
	{
		return one;
	}
	
	U second()
	{
		return two;
	}
}